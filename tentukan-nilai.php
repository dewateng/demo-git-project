<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        function tentukan_nilai($number){
            if ($number>=85 && $number<=100) {
                print ("sangat baik<br>");
            }
            elseif ($number>=70 && $number<85) {
                print ("baik<br>");
            }
            elseif ($number>=60 && $number<70) {
                print ("sangat baik<br>");
            }
            elseif ($number<60){
                print "kurang<br>";
            }
            else{
                print "tidak terjangkau!";
            }
        }
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>